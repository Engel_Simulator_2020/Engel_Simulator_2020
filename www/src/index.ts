import "./styles.scss";

// only the wasm module needs to be loaded async,
// all other resources might be better loaded synchronously
import("engel-simulator-2020").then((wasm) => {

    wasm.start();

    (window as any).cheats = Object.assign(
        {},
        ...Object.keys(wasm)
            .filter((f) => f.startsWith("cheat_"))
            .map((f) => ({[f.substr(6)]: (wasm as any)[f]}))
    );
});
