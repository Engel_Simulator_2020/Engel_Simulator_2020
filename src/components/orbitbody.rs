use crate::components;

pub struct OrbitBody {
    velocity: nalgebra::Vector2<f32>,
    acceleration: nalgebra::Vector2<f32>,
}

impl OrbitBody {
    pub fn new(vel_x: f32, vel_y: f32) -> OrbitBody {
        OrbitBody {
            velocity: nalgebra::Vector2::new(vel_x, vel_y),
            acceleration: nalgebra::Vector2::new(0.0, 0.0),
        }
    }
}

#[legion::system(for_each)]
pub fn update_movement(pos: &mut components::Position, mov: &mut OrbitBody) {
    let delta = 0.3;
    mov.velocity += mov.acceleration * delta;
    pos.0 += mov.velocity * delta;
}

pub struct Gravity;

#[legion::system(for_each)]
pub fn update_gravity(_: &Gravity, mov: &mut OrbitBody, pos: &components::Position) {
    let sun = nalgebra::Point2::new(600.0, 540.0);

    let distance_vector = sun - pos.0;
    let gravity = 60000.0 / distance_vector.magnitude_squared();
    mov.acceleration = distance_vector.normalize() * gravity;
}
