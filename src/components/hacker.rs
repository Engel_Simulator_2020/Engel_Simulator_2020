pub struct HackerWithMail;

pub struct Hacker {
    pub name: String,
    pub has_mail_to_send: bool,
}

impl Hacker {
    pub fn new(name: String, has_mail_to_send: bool) -> Self {
        Hacker {
            name,
            has_mail_to_send,
        }
    }
}
