#[derive(Clone, Copy)]
pub struct ObstacleBarrier {
    w: f32,
    h: f32,
    // TODO replace with angle (we can have rotated colliders now)
    pub orientation: bool, // true means horizontal, false vertical
}

#[derive(Clone, Copy)]
pub struct ObstacleInsanity {
    pub r: f32,
}

impl ObstacleInsanity {
    pub fn new() -> ObstacleInsanity {
        ObstacleInsanity { r: 150.0 }
    }
}

impl ObstacleBarrier {
    pub fn new(orientation: bool) -> ObstacleBarrier {
        ObstacleBarrier {
            w: 200.0,
            h: 20.0,
            orientation,
        }
    }

    pub fn width(&self) -> f32 {
        if self.orientation {
            self.w
        } else {
            self.h
        }
    }
    pub fn height(&self) -> f32 {
        if self.orientation {
            self.h
        } else {
            self.w
        }
    }
}
