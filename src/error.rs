use anyhow::Result;
use wasm_bindgen::JsValue;
use web_sys;

pub trait AnyhowWebExt<T> {
    fn to_anyhow(self) -> anyhow::Result<T>;
}

impl<T> AnyhowWebExt<T> for Result<T, JsValue> {
    fn to_anyhow(self) -> anyhow::Result<T> {
        self.map_err(|js_value| {
            let error_message = match js_value.as_string() {
                Some(string) => string,
                None => "Unknown error".to_string(),
            };
            anyhow::anyhow!("{}", error_message)
        })
    }
}

impl<T> AnyhowWebExt<T> for Result<T, web_sys::Element> {
    fn to_anyhow(self) -> anyhow::Result<T> {
        self.map_err(|js_value| {
            let error_message = match js_value.as_string() {
                Some(string) => string,
                None => "Unknown error".to_string(),
            };
            anyhow::anyhow!("{}", error_message)
        })
    }
}

pub fn handle_system_result<F>(mut f: F)
where
    F: FnMut() -> anyhow::Result<()>,
{
    if let Err(err) = f() {
        panic!("{}", err.to_string());
    }
}
