pub struct Camera {
    pub position: nalgebra::Point2<f64>,
    pub size: nalgebra::Vector2<f64>,
}

impl Camera {
    pub fn new(width: f64, height: f64) -> Self {
        Camera {
            position: nalgebra::Point2::new(0.0, 0.0),
            size: nalgebra::Vector2::new(width, height),
        }
    }

    pub fn draw_coordinate(&self) -> nalgebra::Point2<f64> {
        -(self.position + self.size / 2.0)
    }
}
