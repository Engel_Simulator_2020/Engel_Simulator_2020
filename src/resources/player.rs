/// Stores the entity ID of the player
pub struct Player(pub legion::Entity);
