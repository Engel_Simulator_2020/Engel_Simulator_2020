pub mod bottle_angel;
pub mod chaospost;
mod definitions;
pub mod network_switch;

pub use definitions::AngelShift;
pub use definitions::AngelShiftImpl;
pub use definitions::ShiftMetadata;

pub fn generate_random_shift(rng: &mut impl rand::Rng) -> anyhow::Result<AngelShift> {
    Ok(AngelShift(match rng.gen_range(0usize, 3) {
        0 => Box::new(bottle_angel::BottleAngelShift::generate(rng)?),
        1 => Box::new(network_switch::NetworkSwitchShift::generate(rng)?),
        2 => Box::new(chaospost::ChaosPostShift::generate(rng)?),
        _ => unreachable!(),
    }))
}
