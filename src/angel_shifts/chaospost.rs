use crate::colliders;
use crate::components;
use crate::entities;
use crate::resources;
use crate::svg_loader;
use crate::utils;

use anyhow::Context as _;

pub struct ChaosPostShift {
    hours: usize,
    level: String,
}

impl ChaosPostShift {
    pub fn generate(rng: &mut impl rand::Rng) -> anyhow::Result<ChaosPostShift> {
        use rand::seq::SliceRandom;

        let compatible_levels = ["ccl-ground-level.svg"];

        Ok(ChaosPostShift {
            hours: rng.gen_range(4, 8),
            level: compatible_levels.choose(rng).unwrap().to_string(),
        })
    }
}

impl super::AngelShiftImpl for ChaosPostShift {
    fn metadata(&self) -> anyhow::Result<super::ShiftMetadata> {
        Ok(super::ShiftMetadata {
            title: "Chaos Post".to_owned(),
            description: "Deliver mail to hackers across congress.".to_owned(),
            hours: self.hours,
        })
    }

    fn level_name(&self) -> &str {
        &self.level
    }

    fn init_gameworld(
        &self,
        world: &mut legion::World,
        resources: &mut legion::Resources,
        schedule_builder: &mut legion::systems::Builder,
        level: &svg_loader::SvgLevel,
    ) -> anyhow::Result<()> {
        let hacker_with_mail = entities::create_hackers(world, level);

        // Display objective
        let objective = utils::get_element_by_id::<web_sys::Element>("ingame-objective")?;
        let mut inner_html =
            r#"<text x="30" y="0" class="stats-label">Deliver mail to</text>"#.to_string();

        for (i, hacker) in hacker_with_mail.iter().enumerate() {
            inner_html += &format!(
                r#"<text x="40" y="{ypos}" class="stats-label">{name}:</text>
                <text id="ingame-hacker-stat-{name}" x="460" y="{ypos}" class="stats-number">Pending</text>"#,
                ypos = (i + 1) * 36,
                name = hacker,
            );
        }

        objective.set_inner_html(&inner_html);

        resources.insert(ChaosPostState {
            outstanding_mail: hacker_with_mail.len(),
        });

        schedule_builder
            .add_thread_local(deliver_mail_system())
            .add_thread_local(update_chaospost_shift_system(self.hours));

        Ok(())
    }
}

pub struct ChaosPostState {
    outstanding_mail: usize,
}

#[macro_impl::angel_system]
#[read_component(colliders::Collider)]
#[read_component(components::HackerWithMail)]
#[write_component(components::Hacker)]
pub fn deliver_mail(
    world: &mut legion::world::SubWorld,
    #[resource] player: &resources::Player,
    #[resource] collision_world: &colliders::CollisionWorld,
    #[resource] chaospost_state: &mut ChaosPostState,
) -> anyhow::Result<()> {
    use legion::IntoQuery;

    let collider = <&colliders::Collider>::query()
        .get(world, player.0)
        .context("Couldn't find player")?;

    let mut hackers_with_mail = <(&components::HackerWithMail, &mut components::Hacker)>::query();
    for pair in collision_world
        .world
        .proximities_with(collider.handle.context("No collider handler")?, false)
        .context("Proximities not found")?
    {
        if pair.3 != ncollide2d::query::Proximity::Intersecting {
            continue;
        }

        let entity = *collision_world
            .world
            .objects
            .get(pair.1)
            .context("Couldn't find counterpart")?
            .data();

        if let Ok((_, hacker)) = hackers_with_mail.get_mut(world, entity) {
            if hacker.has_mail_to_send {
                if let Ok(el) = utils::get_element_by_id::<web_sys::Element>(&format!(
                    "ingame-hacker-stat-{}",
                    hacker.name
                )) {
                    el.set_text_content(Some("Done!"));
                    chaospost_state.outstanding_mail -= 1;
                    hacker.has_mail_to_send = false;
                }
            }
        }
    }
    Ok(())
}

#[macro_impl::angel_system]
#[write_component(components::Player)]
pub fn update_chaospost_shift(
    #[state] hours_to_award: &mut usize,
    world: &mut legion::world::SubWorld,
    #[resource] player: &mut resources::Player,
    #[resource] chaospost_state: &ChaosPostState,
    #[resource] game_manager: &mut resources::GameManager,
) -> anyhow::Result<()> {
    use legion::IntoQuery;

    if chaospost_state.outstanding_mail == 0 {
        let player = <&mut components::Player>::query()
            .get_mut(world, player.0)
            .context("Player not found")?;
        player.collected_hours += *hours_to_award as u32;
        *hours_to_award = 0;
        game_manager.request_return_to_heaven();
    }
    Ok(())
}
