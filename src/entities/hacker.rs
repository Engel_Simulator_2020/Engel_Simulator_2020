use crate::colliders;
use crate::components;
use crate::sprites;
use crate::svg_loader;

use rand::seq::SliceRandom;

const HACKER_NAMES: [&str; 30] = [
    "r3v3r53",
    "qu4k3",
    "p4n7h3r",
    "4w3",
    "c0wb0y",
    "fluk3",
    "d0pp3l64n63r",
    "7w157",
    "l364cy",
    "fury",
    "6r1ff1n",
    "5qu1r7",
    "c0wb0y",
    "5l1v3r",
    "7r1x",
    "duckl1n6",
    "470m",
    "ch453",
    "d4663r",
    "1d0l",
    "74l15m4n",
    "4w3",
    "570rm",
    "bl155",
    "5pl1n73r",
    "l16h7n1n6",
    "bull37",
    "v31l",
    "m47r14rch",
    "7r41l",
];

const NUM_HACKERS: usize = 6;
const NUM_HACKER_WITH_MAIL: usize = 3;

pub fn create_hackers(
    world: &mut legion::World,
    level: &svg_loader::SvgLevel,
) -> Vec<&'static str> {
    let spawn_locations = level
        .spawnpoints
        .get("hacker")
        .expect("no hacker spawn in this map");
    if spawn_locations.len() < NUM_HACKERS {
        panic!("to few hacker spawns");
    }

    let positions = spawn_locations.choose_multiple(&mut rand::thread_rng(), NUM_HACKERS);
    let mut names: Vec<&str> = HACKER_NAMES
        .choose_multiple(&mut rand::thread_rng(), NUM_HACKERS)
        .copied()
        .collect();
    for (i, (pos, name)) in positions.zip(names.iter()).enumerate() {
        if i < NUM_HACKER_WITH_MAIL {
            world.push((
                components::Hacker::new(name.to_string(), true),
                components::Position::new(pos.x, pos.y),
                colliders::Collider::new_sensor_circle(250.0),
                components::Sprite::new(sprites::Sprite::Hacker),
                components::HackerWithMail,
            ));
        } else {
            world.push((
                components::Hacker::new(name.to_string(), false),
                components::Position::new(pos.x, pos.y),
                colliders::Collider::new_sensor_circle(250.0),
                components::Sprite::new(sprites::Sprite::Hacker),
            ));
        }
    }

    names.truncate(NUM_HACKER_WITH_MAIL);
    names
}
