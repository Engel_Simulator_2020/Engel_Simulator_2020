use crate::colliders;
use crate::components;
use crate::sprites;
use crate::svg_loader;

use rand::seq::SliceRandom;

pub fn create_player(
    world: &mut legion::World,
    level: &svg_loader::SvgLevel,
    player: components::Player,
) -> legion::Entity {
    let spawn_locations = level
        .spawnpoints
        .get("player")
        .expect("no player spawns in this map");
    let spawn = spawn_locations.choose(&mut rand::thread_rng()).unwrap();

    world.push((
        player,
        components::Position::new(spawn.x, spawn.y),
        components::Movable::new(),
        colliders::Collider::new_player(50.0),
        components::Sprite::new(sprites::Sprite::Player),
    ))
}
