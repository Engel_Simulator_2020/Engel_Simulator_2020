use crate::colliders;
use crate::components;
use crate::sprites;
use crate::svg_loader;
use rand::seq::SliceRandom;

pub fn create_stationary_obstacles(
    world: &mut legion::World,
    level: &svg_loader::SvgLevel,
    player: &components::Player,
) {
    let spawn_locations_barriers = level
        .spawnpoints
        .get("stationary_obstacle_type_1")
        .map_or([].as_ref(), |x| x.as_ref());
    for obs in spawn_locations_barriers.choose_multiple(&mut rand::thread_rng(), 3) {
        let orientation = *[true, false].choose(&mut rand::thread_rng()).unwrap();
        let new_obs = components::ObstacleBarrier::new(orientation);
        world.push((
            new_obs,
            components::Position::new(obs.x, obs.y),
            colliders::Collider::new_static_rect(new_obs.width(), new_obs.height()),
        ));
    }

    if player.difficulty > 0.25 {
        let spawn_locations_insanity = level
            .spawnpoints
            .get("stationary_obstacle_type_2")
            .map_or([].as_ref(), |x| x.as_ref());
        for obs in spawn_locations_insanity.choose_multiple(&mut rand::thread_rng(), 1) {
            let new_obs = components::ObstacleInsanity::new();
            world.push((
                new_obs,
                components::Position::new(obs.x, obs.y),
                colliders::Collider::new_sensor_circle(new_obs.r),
                components::Sprite::new(sprites::Sprite::TrojanHorse),
            ));
        }
    }
}
