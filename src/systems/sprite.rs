use crate::components;
use crate::resources;
use anyhow::Context;

#[macro_impl::angel_system(for_each)]
#[read_component(components::Position)]
#[read_component(components::Sprite)]
pub fn draw_sprites(
    #[resource] rendering: &resources::Rendering,
    position: &components::Position,
    sprite: &components::Sprite,
) -> anyhow::Result<()> {
    let spritesize = rendering
        .get_image_size(&sprite.image_handle)
        .context("Couldn't get image size")?;
    let upper_left = position.0 - spritesize / 2.0 + sprite.offset;
    rendering.draw_image(
        &sprite.image_handle,
        upper_left.x as f64,
        upper_left.y as f64,
    )?;
    Ok(())
}
