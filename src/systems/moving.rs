use crate::colliders;
use crate::components;
use crate::resources;
use legion::IntoQuery;

use anyhow::Context;

#[macro_impl::angel_system]
#[read_component(components::Position)]
pub fn move_camera_to_player(
    world: &legion::world::SubWorld,
    #[resource] camera: &mut resources::Camera,
    #[resource] player: &resources::Player,
) -> anyhow::Result<()> {
    let mut positions = <&components::Position>::query();
    let pos = positions
        .get(world, player.0)
        .context("Player not found")?
        .0;
    camera.position = nalgebra::Point2::new(pos.x as f64, pos.y as f64);
    Ok(())
}

#[macro_impl::angel_system(for_each)]
pub fn move_movable(
    position: &mut components::Position,
    movable: &mut components::Movable,
    collider: &colliders::Collider,
    #[resource] clock: &resources::Clock,
    #[resource] collision_world: &colliders::CollisionWorld,
) -> anyhow::Result<()> {
    let mut actual_velocity = movable.velocity;

    if actual_velocity == nalgebra::Vector2::new(0.0, 0.0) {
        return Ok(());
    }

    // Collect the normals of all collisions we're currently involved in.  These define which
    // directions we can move:  No movement is allowed which has any component pointing towards any
    // of the normals.  In mathematical terms: The dot-product of the final velocity and each of
    // the normals must be 0 or less.
    let normals: Vec<nalgebra::Unit<nalgebra::Vector2<f32>>> = collision_world
        .world
        .contacts_with(collider.handle.context("Collider handle is None")?, false)
        .context("No colliders found")?
        .filter_map(|pair| pair.3.deepest_contact())
        .map(|contact| contact.contact.normal)
        .collect();

    for normal in normals.iter() {
        // Linearly decompose the velocity into the collision normal and collision tangent.  If the
        // component towards the collision normal is greater than 0, remove it and only keep the
        // tangential component.
        let d: f32 = normal.dot(&actual_velocity);
        if d > 0.0 {
            actual_velocity -= normal.into_inner() * d;
        }
    }

    // If the final velocity has _no_ component towards the initial velocity, we'd be moving
    // backwards ... Prevent this.
    if actual_velocity.dot(&movable.velocity) < 0.0 {
        return Ok(());
    }

    for normal in normals.iter() {
        // If the final velocity has a components into any of the normals, we are in a dead end and
        // should not move at all.
        if normal.dot(&actual_velocity) > 0.02 {
            return Ok(());
        }
    }

    position.0 += actual_velocity * clock.frame_delta();
    Ok(())
}
