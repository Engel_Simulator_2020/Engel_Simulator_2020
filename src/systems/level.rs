use crate::resources;

#[legion::system]
pub fn draw_level_layer(
    #[state] handle: &resources::ImageHandle,
    #[resource] rendering: &resources::Rendering,
) {
    match rendering.draw_image(handle, 0.0, 0.0) {
        Ok(_) => {}
        Err(err) => crate::console_log!("Error drawing level layer: {}", err),
    }
}
