use crate::colliders;
use crate::components;
use crate::resources;

use anyhow::Context;
use legion::IntoQuery;

#[macro_impl::angel_system]
#[read_component(components::Position)]
#[read_component(components::Hacker)]
#[read_component(colliders::Collider)]
pub fn draw_hacker_name(
    world: &legion::world::SubWorld,
    #[resource] rendering: &resources::Rendering,
    #[resource] player: &resources::Player,
    #[resource] collision_world: &colliders::CollisionWorld,
) -> anyhow::Result<()> {
    let player_collider = <&colliders::Collider>::query()
        .get(world, player.0)
        .context("Couldn't find player")?;

    rendering.set_fill_style(&resources::Color::with_rgb(1.0, 1.0, 1.0));

    let mut hackers = <(&components::Position, &components::Hacker)>::query();
    for pair in collision_world
        .world
        .proximities_with(
            player_collider.handle.context("No collider handler")?,
            false,
        )
        .context("Proximities not found")?
    {
        if pair.3 != ncollide2d::query::Proximity::Intersecting {
            continue;
        }

        let entity = *collision_world
            .world
            .objects
            .get(pair.1)
            .context("Couldn't find counterpart")?
            .data();
        if let Ok((position, hacker)) = hackers.get(world, entity) {
            rendering.fill_text(position.0.x as f64, position.0.y as f64, &hacker.name, 50.0)?;
        }
    }

    Ok(())
}
