use crate::colors;
use crate::components;
use crate::error::AnyhowWebExt;
use crate::gamestate;
use crate::resources;
use crate::utils;
use anyhow::Context;

pub struct WinState {
    gui_svg: web_sys::SvgElement,
    node_world: legion::World,
    resources: legion::Resources,
    node_schedule: legion::Schedule,
}

impl WinState {
    pub fn new(_player: Option<components::Player>) -> anyhow::Result<WinState> {
        let (node_world, resources, node_schedule) = init_orbiting_nodes()?;

        Ok(WinState {
            gui_svg: utils::get_element_by_id("win-ui")?,
            node_world,
            resources,
            node_schedule,
        })
    }
}

impl gamestate::State for WinState {
    fn init(
        &mut self,
        mut _init: gamestate::StateInitializer,
    ) -> anyhow::Result<gamestate::Transition> {
        self.gui_svg
            .style()
            .set_property("display", "block")
            .to_anyhow()?;

        let token = option_env!("BADGE_REDEEM_TOKEN").unwrap_or("uhagre7");

        let token_decoded: String = token
            .chars()
            .map(|c| match c {
                'A'..='M' | 'a'..='m' => ((c as u8) + 13) as char,
                'N'..='Z' | 'n'..='z' => ((c as u8) - 13) as char,
                '0'..='9' => (0x39 - (c as u8) + 0x30) as char,
                _ => c,
            })
            .collect();

        let token_field = utils::get_element_by_id::<web_sys::Element>("win-token")?;
        token_field.set_inner_html(&format!(
            r#"
            <a href="https://rc3.world/2021/me/redeem_badge/{token}">
                <g id="game-over-replay" class="huge-button button-1">
                    <rect x="-350" y="0" width="700" height="120" />
                    <text x="0" y="85">Claim your token</text>
                </g>
            </a>
            "#,
            token = token_decoded
        ));

        Ok(gamestate::Transition::Loop)
    }

    fn deinit(&mut self) -> anyhow::Result<()> {
        self.gui_svg
            .style()
            .set_property("display", "none")
            .to_anyhow()
    }

    fn update(&mut self, timestamp: f64) -> anyhow::Result<gamestate::Transition> {
        self.resources
            .get_mut::<resources::Clock>()
            .context("Could not find Clock resource")?
            .update(timestamp);

        {
            let rendering = self
                .resources
                .get_mut::<resources::Rendering>()
                .context("Could not get resource 'Rendering'")?;
            rendering.set_fill_style(&colors::BACKGROUND);
            rendering.fill_rect(0.0, 0.0, 1920.0, 1080.0);
        }

        self.node_schedule
            .execute(&mut self.node_world, &mut self.resources);

        Ok(gamestate::Transition::Loop)
    }

    fn event(&mut self, event: gamestate::Event) -> anyhow::Result<gamestate::Transition> {
        crate::console_log!("unknown event {:?}", event);
        Ok(gamestate::Transition::Keep)
    }
}

fn init_orbiting_nodes() -> anyhow::Result<(legion::World, legion::Resources, legion::Schedule)> {
    let mut resources = legion::Resources::default();
    resources.insert(resources::Clock::new());
    resources.insert(resources::Rendering::new("game-canvas")?);

    let mut node_world = legion::World::default();
    let n0 = node_world.push((
        components::Node::new(),
        components::Position::new(800.0, 340.0),
        components::OrbitBody::new(5.0, 10.0),
        components::Gravity,
    ));
    let n1 = node_world.push((
        components::Node::new(),
        components::Position::new(120.0, 300.0),
        components::OrbitBody::new(5.0, -5.0),
        components::Gravity,
    ));
    let n2 = node_world.push((
        components::Node::new(),
        components::Position::new(700.0, 740.0),
        components::OrbitBody::new(8.0, -4.0),
        components::Gravity,
    ));
    let n3 = node_world.push((
        components::Node::new(),
        components::Position::new(340.0, 290.0),
        components::OrbitBody::new(10.0, 0.0),
        components::Gravity,
    ));
    let n4 = node_world.push((
        components::Node::new(),
        components::Position::new(300.0, 400.0),
        components::OrbitBody::new(0.0, 10.0),
        components::Gravity,
    ));

    node_world.push((components::Edge::new(n0, n1),));
    node_world.push((components::Edge::new(n1, n2),));
    node_world.push((components::Edge::new(n0, n2),));
    node_world.push((components::Edge::new(n2, n3),));
    node_world.push((components::Edge::new(n3, n4),));
    node_world.push((components::Edge::new(n0, n4),));

    node_world.push((components::TheSun, components::Position::new(600.0, 540.0)));

    let node_schedule = legion::Schedule::builder()
        .add_system(components::update_gravity_system())
        .add_system(components::update_movement_system())
        .add_thread_local(components::update_nodes_system())
        .flush()
        .add_thread_local(components::draw_edges_system())
        .add_thread_local(components::draw_nodes_system())
        .add_thread_local(components::draw_thesun_system())
        .build();

    Ok((node_world, resources, node_schedule))
}
