use crate::angel_shifts;
use crate::cheats;
use crate::colors;
use crate::components;
use crate::error::AnyhowWebExt;
use crate::gamestate;
use crate::resources;
use crate::states;
use crate::utils;
use anyhow::Context;

pub struct HeavenState {
    gui_svg: web_sys::SvgElement,

    node_world: legion::World,
    resources: legion::Resources,
    node_schedule: legion::Schedule,

    player: components::Player,
    assigned_shift: Option<angel_shifts::AngelShift>,
}

impl HeavenState {
    pub fn new(player: Option<components::Player>) -> anyhow::Result<HeavenState> {
        let (node_world, resources, node_schedule) = init_orbiting_nodes()?;

        let mut player = player.unwrap_or_else(|| {
            // Initialize the game as this is the first run.
            components::Player::new()
        });

        // Update the angel stats GUI elements
        utils::get_element_by_id::<web_sys::Element>("heaven-sanity")?
            .set_inner_html(&format!("{}%", (player.sanity * 100.0).round() as usize));
        utils::get_element_by_id::<web_sys::Element>("heaven-collected-hours")?
            .set_inner_html(&format!("{}", player.collected_hours));
        if player.collected_hours < 30 {
            utils::get_element_by_id::<web_sys::Element>("heaven-needed-hours")?
                .set_inner_html(&format!("{}", 30 - player.collected_hours));
        } else {
            utils::get_element_by_id::<web_sys::Element>("heaven-needed-hours")?
                .set_inner_html("None!");
        }

        // Assign a random shift
        let assigned_shift = angel_shifts::generate_random_shift(&mut rand::thread_rng())?;

        // Decide difficulty for the next shift
        player.difficulty = ((player.shifts_completed as f32 + 1.0) * 0.1).min(1.0);

        // Display the shift info
        let shift_meta = assigned_shift.metadata()?;
        utils::get_element_by_id::<web_sys::Element>("heaven-shift-title")?
            .set_inner_html(&shift_meta.title);
        utils::get_element_by_id::<web_sys::Element>("heaven-shift-description")?
            .set_inner_html(&shift_meta.description);
        utils::get_element_by_id::<web_sys::Element>("heaven-shift-hours")?
            .set_inner_html(&format!("{}", shift_meta.hours));

        Ok(HeavenState {
            gui_svg: utils::get_element_by_id("heaven-ui")?,
            node_world,
            resources,
            node_schedule,
            player,
            assigned_shift: Some(assigned_shift),
        })
    }
}

impl gamestate::State for HeavenState {
    fn init(
        &mut self,
        mut init: gamestate::StateInitializer,
    ) -> anyhow::Result<gamestate::Transition> {
        init.register_onclick("heaven-start-shift")?;
        self.gui_svg
            .style()
            .set_property("display", "block")
            .to_anyhow()?;
        if self.player.collected_hours >= 30 {
            Ok(gamestate::Transition::replace(states::WinState::new(
                Some(self.player.clone()),
            )?))
        } else {
            Ok(gamestate::Transition::Loop)
        }
    }

    fn deinit(&mut self) -> anyhow::Result<()> {
        self.gui_svg
            .style()
            .set_property("display", "none")
            .to_anyhow()
    }

    fn update(&mut self, timestamp: f64) -> anyhow::Result<gamestate::Transition> {
        self.resources
            .get_mut::<resources::Clock>()
            .context("Could not find resource 'Clock'")?
            .update(timestamp);

        {
            let rendering = self
                .resources
                .get_mut::<resources::Rendering>()
                .context("Could not find resource 'Rendering'")?;
            rendering.set_fill_style(&colors::BACKGROUND);
            rendering.fill_rect(0.0, 0.0, 1920.0, 1080.0);
        }

        self.node_schedule
            .execute(&mut self.node_world, &mut self.resources);

        Ok(gamestate::Transition::Loop)
    }

    fn event(&mut self, event: gamestate::Event) -> anyhow::Result<gamestate::Transition> {
        Ok(match event {
            gamestate::Event::MouseClick {
                target: "heaven-start-shift",
                ..
            } => gamestate::Transition::replace(states::LevelLoadingState::new(
                self.player.clone(),
                self.assigned_shift.take().context("No shift was defined")?,
            )?),
            gamestate::Event::Cheat(cheats::CheatCommand::SetSanity(val)) => {
                self.player.sanity = val;
                gamestate::Transition::Keep
            }
            gamestate::Event::Cheat(cheats::CheatCommand::SetShifts(val)) => {
                self.player.shifts_completed = val;
                gamestate::Transition::Keep
            }
            gamestate::Event::Cheat(cheats::CheatCommand::GetPlayer()) => {
                crate::console_warn!("Player: {:?}", self.player);
                gamestate::Transition::Keep
            }
            event => {
                crate::console_warn!("unknown event: {:?}", event);
                gamestate::Transition::Keep
            }
        })
    }
}

fn init_orbiting_nodes() -> anyhow::Result<(legion::World, legion::Resources, legion::Schedule)> {
    let mut resources = legion::Resources::default();
    resources.insert(resources::Clock::new());
    resources.insert(resources::Rendering::new("game-canvas")?);

    let mut node_world = legion::World::default();
    let n0 = node_world.push((
        components::Node::new(),
        components::Position::new(800.0, 340.0),
        components::OrbitBody::new(5.0, 10.0),
        components::Gravity,
    ));
    let n1 = node_world.push((
        components::Node::new(),
        components::Position::new(120.0, 300.0),
        components::OrbitBody::new(5.0, -5.0),
        components::Gravity,
    ));
    let n2 = node_world.push((
        components::Node::new(),
        components::Position::new(700.0, 740.0),
        components::OrbitBody::new(8.0, -4.0),
        components::Gravity,
    ));
    let n3 = node_world.push((
        components::Node::new(),
        components::Position::new(340.0, 290.0),
        components::OrbitBody::new(10.0, 0.0),
        components::Gravity,
    ));
    let n4 = node_world.push((
        components::Node::new(),
        components::Position::new(300.0, 400.0),
        components::OrbitBody::new(0.0, 10.0),
        components::Gravity,
    ));

    node_world.push((components::Edge::new(n0, n1),));
    node_world.push((components::Edge::new(n1, n2),));
    node_world.push((components::Edge::new(n0, n2),));
    node_world.push((components::Edge::new(n2, n3),));
    node_world.push((components::Edge::new(n3, n4),));
    node_world.push((components::Edge::new(n0, n4),));

    node_world.push((components::TheSun, components::Position::new(600.0, 540.0)));

    let node_schedule = legion::Schedule::builder()
        .add_system(components::update_gravity_system())
        .add_system(components::update_movement_system())
        .add_thread_local(components::update_nodes_system())
        .flush()
        .add_thread_local(components::draw_edges_system())
        .add_thread_local(components::draw_nodes_system())
        .add_thread_local(components::draw_thesun_system())
        .build();

    Ok((node_world, resources, node_schedule))
}
