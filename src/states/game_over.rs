use crate::error::AnyhowWebExt;
use crate::gamestate;
use crate::states;
use crate::utils;

pub struct GameOverState {
    gui_svg: web_sys::SvgElement,
}

impl GameOverState {
    pub fn new() -> anyhow::Result<GameOverState> {
        Ok(GameOverState {
            gui_svg: utils::get_element_by_id("game-over-ui")?,
        })
    }
}

impl gamestate::State for GameOverState {
    fn init(
        &mut self,
        mut init: gamestate::StateInitializer,
    ) -> anyhow::Result<gamestate::Transition> {
        init.register_onclick("game-over-replay")?;
        init.register_onclick("game-over-quit")?;
        self.gui_svg
            .style()
            .set_property("display", "block")
            .to_anyhow()?;

        Ok(gamestate::Transition::Sleep)
    }

    fn deinit(&mut self) -> anyhow::Result<()> {
        self.gui_svg
            .style()
            .set_property("display", "none")
            .to_anyhow()
    }

    fn event(&mut self, event: gamestate::Event) -> anyhow::Result<gamestate::Transition> {
        Ok(match event {
            gamestate::Event::MouseClick {
                target: "game-over-replay",
                ..
            } => gamestate::Transition::push(states::HeavenState::new(None)?),
            gamestate::Event::MouseClick {
                target: "game-over-quit",
                ..
            } => gamestate::Transition::Pop,
            event => {
                crate::console_warn!("unknown event {:?}", event);
                gamestate::Transition::Keep
            }
        })
    }
}
