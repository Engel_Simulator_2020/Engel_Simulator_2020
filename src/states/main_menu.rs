use crate::error::AnyhowWebExt;
use crate::gamestate;
use crate::states;
use crate::utils;

pub struct MainMenuState {
    gui_svg: web_sys::SvgElement,
}

impl MainMenuState {
    pub fn new() -> anyhow::Result<MainMenuState> {
        Ok(MainMenuState {
            gui_svg: utils::get_element_by_id("menu-ui")?,
        })
    }
}

impl gamestate::State for MainMenuState {
    fn init(
        &mut self,
        mut init: gamestate::StateInitializer,
    ) -> anyhow::Result<gamestate::Transition> {
        init.register_onclick("main-menu-play")?;
        self.gui_svg
            .style()
            .set_property("display", "block")
            .to_anyhow()?;

        // In debug builds, skip the main menu and go straight to heaven.  This eases iterative
        // development because one does not need to click "Play Game" every time ...
        Ok(if cfg!(debug_assertions) {
            gamestate::Transition::push(states::HeavenState::new(None)?)
        } else {
            gamestate::Transition::Sleep
        })
    }

    fn deinit(&mut self) -> anyhow::Result<()> {
        self.gui_svg
            .style()
            .set_property("display", "none")
            .to_anyhow()
    }

    fn event(&mut self, event: gamestate::Event) -> anyhow::Result<gamestate::Transition> {
        Ok(match event {
            gamestate::Event::MouseClick {
                target: "main-menu-play",
                ..
            } => gamestate::Transition::push(states::HeavenState::new(None)?),
            event => {
                crate::console_log!("unknown event {:?}", event);
                gamestate::Transition::Keep
            }
        })
    }
}
