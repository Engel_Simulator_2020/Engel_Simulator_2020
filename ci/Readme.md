# Setup

## Install and register runnner
- Install GitLab runner
  - See https://docs.gitlab.com/runner/install/
- Register using `gitlab-runner register`
  - See https://docs.gitlab.com/runner/register/
  - Use `docker` executor
- Register a second, protected runner
  - Use `shell` executor
  - Tag it as `local`
  - Set it as protected (see https://docs.gitlab.com/ee/ci/runners/README.html#prevent-runners-from-revealing-sensitive-information)

## Build local docker image
To speed up the build, we use a customized docker image.
The easiest way is to build the image locally:

- Clone repo to runner machine:
`git clone https://gitlab.muc.ccc.de/engel-simulator-2020/game.git`

- Build docker image:
`docker build game/ci -t engel-simulator-builder`

## Enable local docker images
The GitLab runner will always try to pull images from docker hub by default. To use local images, you must enable this in `/etc/gitlab-runner/config.toml`.

Here, add `pull_policy = "if-not-present"` to the `[runners.docker]` section.


