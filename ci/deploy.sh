#!/bin/bash
set -e

INDIR="$1"
OUTDIR="$2"

if ! test -h "$OUTDIR/game" && test -d "$OUTDIR/game"; then
    echo "Removing legacy deployment directory ..."
    rm -r "$OUTDIR/game"
fi

if ! test -d "$OUTDIR/versions"; then
    echo "Creating versions directory ..."
    mkdir "$OUTDIR/versions"
fi

VERSION="$(git -C "$INDIR" describe --always --long)"

echo "Copying to $OUTDIR/versions/$VERSION ..."
cp -rT "$INDIR" "$OUTDIR/versions/$VERSION"

echo "Atomically updating symlink ..."
ln -nsf "$OUTDIR/versions/$VERSION" "$OUTDIR/new-game"
mv -T "$OUTDIR/new-game" "$OUTDIR/game"

echo "WE ARE LIVE!"

echo "Cleaning up previous deployments ..."
for PREVIOUS in "$OUTDIR/versions"/*; do
    if ! [ "$PREVIOUS" = "$OUTDIR/versions/$VERSION" ]; then
        echo " - Removing $PREVIOUS ..."
        rm -r "$PREVIOUS"
    fi
done
