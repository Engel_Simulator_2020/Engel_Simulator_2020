extern crate proc_macro;

use proc_macro::TokenStream;

#[proc_macro_attribute]
pub fn angel_system(args: TokenStream, input: TokenStream) -> TokenStream {
    let f = syn::parse_macro_input!(input as syn::ItemFn);

    let fname = f.sig.ident;
    let attrs = f.attrs;
    let inputs = f.sig.inputs;
    let block = f.block;
    let stmts = block.stmts;
    let rtype = f.sig.output;
    let vis = f.vis;

    let legion_attr = if !args.is_empty() {
        let args: proc_macro2::TokenStream = args.into();
        quote::quote!(#[legion::system(#args)])
    } else {
        quote::quote!(#[legion::system])
    };

    quote::quote!(
        #legion_attr
        #(#attrs)*
        #vis fn #fname(#inputs) {
            crate::error::handle_system_result(|| #rtype {
                #(#stmts)*
            });
        }
    )
    .into()
}

#[proc_macro_attribute]
pub fn wasm_bindgen_anyhow(_args: TokenStream, input: TokenStream) -> TokenStream {
    let f = syn::parse_macro_input!(input as syn::ItemFn);

    let fname = f.sig.ident;
    let attrs = f.attrs;
    let inputs = f.sig.inputs;
    let block = f.block;
    let stmts = block.stmts;
    let rtype = f.sig.output;
    let vis = f.vis;

    quote::quote!(
        #[wasm_bindgen]
        #(#attrs)*
        #vis fn #fname(#inputs) -> Result<(), wasm_bindgen::JsValue> {
            match || #rtype {
                #(#stmts)*
            }() {
                Ok(()) => Ok(()),
                Err(err) => Err(
                    wasm_bindgen::JsValue::from_str(&format!("{}", err))
                ),
            }
        }
    )
    .into()
}
